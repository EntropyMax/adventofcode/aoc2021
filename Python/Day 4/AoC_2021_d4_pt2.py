"""
BINGO! Part 2, find who would be in last place.

Provided:
List of random INTs

A few rows of bingo cards (in bingo_cards_sample.data)

List of 5x5 bingo cards with each coordinate having a list of 2 spots


Parsing the raw data
"""

"""
{<int>:<5x5 card array>}
"""
def parse_the_bingo_cards(file_location, print_me=False):
    """
    file location for data of card information will be parsed into a dictionary
    {<int>:(
    ([1,None],[2,None],[3,None],[4,None],[5,None]),
    ([1,None],[2,None],[3,None],[4,None],[5,None]),
    ([1,None],[2,None],[3,None],[4,None],[5,None]),
    ([1,None],[2,None],[3,None],[4,None],[5,None]),
    ([1,None],[2,None],[3,None],[4,None],[5,None])
    )
    }
    """
    card_index=1
    the_data = {}
    the_card = []
    #open the file
    with open(bingo_cards_raw_data) as fh:
        #look at text data line by line
        for line in fh.readlines():
            # as long as the line is greater than one (new line char)
            if len(line) > 1:
                # split the line into list of numbers
                line2list=line.split()
                # enumerate the list and initialize each value as [itself, unselected]
                for index, value in enumerate(line2list):
                    line2list[index] = [value, None]
                line2list = tuple(line2list)
                # add row to the card
                the_card.append(line2list)
            else:
                # make the card positioning immutable
                the_card = tuple(the_card)
                if print_me:
                    print(the_card)
                the_data[card_index] = the_card
                card_index += 1
                the_card = []
        if print_me:
            print(the_card)
        the_card = tuple(the_card)
        the_data[card_index] = the_card
    if print_me:
        print("--------")
        for key, value in the_data.items():
            print (key,":")
            for value2 in value:
                print(value2)
        print(the_data)
    return the_data

def add_number_picked(selected_value, bingo_card_dictionary, print_me = False):
    """
    The bingo cards are supplied with the picked value as a string.
    Any number that matches will have the "None" updated to "True"

    """
    value_to_apply = selected_value
    if type(value_to_apply) != type(''):
        value_to_apply = str(value_to_apply)
    for bingo_card in bingo_card_dictionary.values():
        if print_me:
            print(bingo_card)
        for bingo_row in bingo_card:
            for row_item in bingo_row:
                if row_item[0] == value_to_apply:
                    row_item[1] = True
        if print_me:
            print(bingo_card)

def check_for_bingo(bingo_card, print_me=False):
    """
    A single bingo card is provided to look for:
    horizontal bingo
    vertical bingo
    returns "true" if bingo is found None otherwize
    """
    # initialize column counter (row will just return True if it gets 5 in a row)
    column_counter = [0,0,0,0,0]
    for row in range(5):
        row_bingo_counter = 0
        for column in range(5):
            try:
                if bingo_card[row][column][1] == True:
                    column_counter[column] += 1
                    row_bingo_counter += 1
                    if row_bingo_counter == 5:
                        if print_me:
                            print(f"\n    !!!ROW -->{row}<-- BINGO!!!\n")
                        return True
            except:
                print(bingo_card)
                assert 1==0
        for value in column_counter:
            if value == 5:
                if print_me:
                    print(f"\n    !!!COLUMN -->{column_counter}<-- BINGO!!!\n")
                return True
            
def sum_true_false_card_values(bingo_card, print_me=False):
    """
    This will iterate through the bingo card with an accumulator true and false values

    """
    true_summator = 0
    false_summator = 0
    for bingo_row in bingo_card:
        for row_item in bingo_row:
            if row_item[1] == True:
                true_summator += int(row_item[0])
            else:
                false_summator += int(row_item[0])
    if print_me:
        print(f"true summator: {true_summator} false summator: {false_summator}")
    return (true_summator, false_summator)

def print_bingo_card(bingo_card):
    """
    print out pretty bingo card
    """
    for row in bingo_card:
        for col in row:
            print(col," ", end='')
        print()

def find_bingo(bingo_cards_in_play, entry_numbers, print_me=False):
    """
    This will find a bingo and pop it from the dictionary
    After poping, it will return:
        new dictionary missing the winning card
        the winning card
        input entry number
        last number that caused the win
        return (bingo_winner_key, winning_bingo_card, index_of_called_number, value_of_that_input)
    """
    bingo_found = None
    for index, number_picked in enumerate(entry_numbers):
        add_number_picked(number_picked, bingo_cards_in_play)
        for key, card in bingo_cards_in_play.items():
            bingo_found = check_for_bingo(card)
            if bingo_found == True:
                # BINGO
                bingo_winner_key = key
                winning_bingo_card = bingo_cards_in_play.pop(bingo_winner_key)
                if print_me:
                    print(f"""
    Bingo on CARD #: {bingo_winner_key} 
    On input entry number {index} 
    With input value: {number_picked}""")
                    print_bingo_card(winning_bingo_card)
                return (bingo_winner_key, winning_bingo_card, index, number_picked)

def get_the_answer_part1(bingo_cards_raw_data, input_list, print_me=False):
    """
    finds the first winning card, sums all unmarked numbers together
    multiplies this number with the number that was just called to win the value
    displays that number
    """
    (bingo_winner_key, winning_bingo_card, index_of_called_number, value_of_that_input) = find_bingo(parse_the_bingo_cards(bingo_cards_raw_data, print_me), input_list, print_me)
    (true_sum, false_sum) = sum_true_false_card_values(winning_bingo_card, print_me)
    if print_me:
        print(false_sum * value_of_that_input)
    return (false_sum * value_of_that_input)

def find_last_place_pt2(bingo_cards_raw_data, input_list, print_me=False):
    """
    Get the last bingo!
    Do this by popping the dictionary until there is only one left.
    """
    bingo_cards = parse_the_bingo_cards(bingo_cards_raw_data, print_me)
    while len(bingo_cards) > 0:
        (bingo_winner_key, winning_bingo_card, index_of_called_number, value_of_that_input) = find_bingo(bingo_cards, input_list, print_me)
    (true_sum, false_sum) = sum_true_false_card_values(winning_bingo_card, print_me)  
    if print_me:
        print(false_sum * value_of_that_input)
    return (false_sum * value_of_that_input)

if __name__ == '__main__':
    bingo_ints = [7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1]
    bingo_cards_raw_data = r'./Python/Day 4/bingo_cards_sample.data'
    parsed_data = parse_the_bingo_cards(bingo_cards_raw_data)
    # original = dict(parsed_data)
    (bingo_winner_key, winning_bingo_card, index_of_called_number, value_of_that_input) = find_bingo(parsed_data, bingo_ints)
    # for key, card in parsed_data.items():
    #     print(key, card)
    #     print()
    # print_bingo_card(winning_bingo_card)
    (true_sum, false_sum) = sum_true_false_card_values(winning_bingo_card)
    the_answer = get_the_answer_part1(bingo_cards_raw_data, bingo_ints)
    assert 4512 == the_answer
    print("Part I sample:", the_answer)

    the_answer = find_last_place_pt2(bingo_cards_raw_data, bingo_ints)
    print("Part II sample:", the_answer)


    bingo_ints = [67,31,58,8,79,18,19,45,38,13,40,62,85,10,21,96,56,55,4,36,76,42,32,34,39,89,6,12,24,57,93,47,41,52,83,61,5,37,28,15,86,23,69,92,70,27,25,53,44,80,65,22,99,43,66,26,11,72,2,98,14,82,87,20,73,46,35,7,1,84,95,74,81,63,78,94,16,60,29,97,91,30,17,54,68,90,71,88,77,9,64,50,0,49,48,75,3,59,51,33]
    bingo_cards_raw_data = r'./Python/Day 4/bingo_cards_puzzle.data'

    the_answer2 = get_the_answer_part1(bingo_cards_raw_data, bingo_ints)
    print("Part I puzzle:", the_answer2)
    the_answer2 = find_last_place_pt2(bingo_cards_raw_data, bingo_ints)
    print("Part II sample:", the_answer2)