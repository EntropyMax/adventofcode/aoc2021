"""
commands:
forward X increases the horizontal position by X units.
down X increases the depth by X units. [deeper (more positive)]
up X decreases the depth by X units. [shallower (less positive)]

test data:
forward 5
down 5
forward 8
up 3
down 8
forward 2


Your horizontal position and depth both start at 0. The steps above would then modify them as follows:

forward 5 adds 5 to your horizontal position, a total of 5.
down 5 adds 5 to your depth, resulting in a value of 5.
forward 8 adds 8 to your horizontal position, a total of 13.
up 3 decreases your depth by 3, resulting in a value of 2.
down 8 adds 8 to your depth, resulting in a value of 10.
forward 2 adds 2 to your horizontal position, a total of 15.
After following these instructions, you would have a horizontal position of 15 and a depth of 10. (Multiplying these together produces 150.)

Calculate the horizontal position and depth you would have after following the planned course. What do you get if you multiply your final horizontal position by your final depth?
"""

class TheSubmarine:
    """
    This represents a submarine object
    """
    def __init__(self, fwd_position=0, depth=0):
        """
        Initialize the object with a starting position of zero
        forward x increase horizontal position
        down x increase depth
        up x decrease depth
        """
        self.horizontal_position = fwd_position
        self.depth = depth

    def course_request(self,requested_movement):
        """
        text input recieved that will need to be parsed:
        example in: "forward 5"
        this will be adjusted to two variables 'forward' and 5
        """
        (movement_direction, movement_request) = requested_movement.split()
        movement_request = int(movement_request)
        movement_direction = movement_direction.lower()
        if movement_direction == 'forward':
            self.horizontal_position += movement_request
        elif (movement_direction == 'up'):
            self.depth -= movement_request
        elif movement_direction == 'down':
            self.depth += movement_request
        if self.depth < 0:
            # submarines can't fly
            self.depth = 0
    
    def get_positions(self):
        print(
        f"""
        Horizontal Position: {self.horizontal_position}
        Depth: {self.depth}
        Multiplied together: {self.depth * self. horizontal_position}
        """)

if __name__ == "__main__":
    testdata =[
        "forward 5",
        "down 5",
        "forward 8",
        "up 3",
        "down 8",
        "forward 2 " 
        ]

    day2_pt1 = TheSubmarine()

    for request in testdata:
        print(request)
        day2_pt1.course_request(request)
    day2_pt1.get_positions()
    if day2_pt1.horizontal_position == 15 and day2_pt1.depth == 10:
        print("day2 part 1 unit test pass")
    else:
        print("Day2 unit test failed")





