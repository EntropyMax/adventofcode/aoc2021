"""
---part 2
commands:
down X increases your aim by X units.
up X decreases your aim by X units.
forward X does two things:
It increases your horizontal position by X units.
It increases your depth by your aim multiplied by X.

test data:
forward 5
down 5
forward 8
up 3
down 8
forward 2


Now, the above example does something different:

forward 5 adds 5 to your horizontal position, a total of 5. Because your aim is 0, your depth does not change.
down 5 adds 5 to your aim, resulting in a value of 5.
forward 8 adds 8 to your horizontal position, a total of 13. Because your aim is 5, your depth increases by 8*5=40.
up 3 decreases your aim by 3, resulting in a value of 2.
down 8 adds 8 to your aim, resulting in a value of 10.
forward 2 adds 2 to your horizontal position, a total of 15. Because your aim is 10, your depth increases by 2*10=20 to a total of 60.
After following these new instructions, you would have a horizontal position of 15 and a depth of 60. (Multiplying these produces 900.)
"""

class TheSubmarine:
    """
    This represents a submarine object
    """
    def __init__(self, fwd_position=0, depth=0, aim=0):
        """
        Initialize the object with a starting position of zero
        forward x increase horizontal position
        down x increase depth
        up x decrease depth
        """
        self.horizontal_position = fwd_position
        self.depth = depth
        self.aim = aim

    def course_request(self,requested_movement):
        """
        text input recieved that will need to be parsed:
        example in: "forward 5"
        this will be adjusted to two variables 'forward' and 5
        """
        (movement_direction, movement_request) = requested_movement.split()
        movement_request = int(movement_request)
        movement_direction = movement_direction.lower()
        if movement_direction == 'forward':
            self.horizontal_position += movement_request
            self.depth += (movement_request * self.aim)
        elif (movement_direction == 'up'):
            self.aim -= movement_request
        elif movement_direction == 'down':
            self.aim += movement_request
        # calculate depth
        if self.depth < 0:
            # submarines can't fly
            self.depth = 0
    

    
    def get_positions(self):
        print(
        f"""
        Horizontal Position: {self.horizontal_position}
        Depth: {self.depth}
        Multiplied together: {self.depth * self. horizontal_position}
        """)

if __name__ == "__main__":
    testdata =[
        "forward 5",
        "down 5",
        "forward 8",
        "up 3",
        "down 8",
        "forward 2 " 
        ]

    day2_pt1 = TheSubmarine()

    for request in testdata:
        print(request)
        day2_pt1.course_request(request)
    day2_pt1.get_positions()
    if day2_pt1.horizontal_position == 15 and day2_pt1.depth == 60:
        print("day2 part 1 unit test pass")
    else:
        print("Day2 unit test failed")





