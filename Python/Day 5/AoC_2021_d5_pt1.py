"""
Given horizontal or vertial lines in a cartian coordinate system

Find all int points that overlap at each point and give a count.

"""
import re
from itertools import chain

def parse_input_coordiantes(file_location, the_data=[], print_me=False):
    """
    Sample data from file:
    0,9 -> 5,9
    8,0 -> 0,8
    9,4 -> 3,4

    each line:
    x1,y1 -> x2,y2

    use regex to get the digits out  '[0-9]+'

    add line coordinates as a list of tuples
    
    """
    #open the file
    with open(file_location) as fh:
        #look at text data line by line
        for line in fh.readlines():
            (x1,y1,x2,y2) = re.findall('[0-9]+',line)
            the_line_points = (int(x1),int(y1)),(int(x2),int(y2))
            the_data.append(the_line_points)
            if print_me:
                for data in the_data:
                    print(data)
    return the_data

def find_hi_lo(the_data):        
    """
    Find largest x and y values 
    """
    xlarge=0
    ylarge=0
    for line in the_data:
        (x1,y1),(x2,y2) = line
        if x1 > xlarge:
            xlarge = x1
        if x2 > xlarge:
            xlarge = x2
        if y1 > ylarge:
            ylarge = y1
        if y2 > ylarge:
            ylarge = y2
    return (xlarge, ylarge)

def create_xy_axis(xlarge, ylarge, print_me=False):
    """
    create a 2d axis list initialized with a value of 0
    """
    initialize_value = 0
    the_axis = [[initialize_value for j in range(xlarge+1)] for i in range(ylarge+1)]
    if print_me:
        print_the_axis(the_axis)
    return the_axis

def print_the_axis(the_axis):
    """
    prints out the axis to the counsol
    """
    for row in the_axis:
        print(row)

def add_line_to_axis(axis,line_endpoints, print_me=False):
    """
    input:
        2D List
        line_endpoints i.e: ((x1, y1), (x2, y2)) 

    function: increment all points between the two points given
    """
    (x1, y1), (x2, y2) = line_endpoints
    if x1 == x2:
        # vertial line
        for _ in range(min(y1,y2),max(y2,y1)+1):
            axis[_][x1] += 1
    elif y1 == y2:
        # horizontal line
        for _ in range(min(x1,x2),max(x1,x2)+1):
            axis[y1][_] += 1
    else:
        if print_me:
            print("bad data provided")
    if print_me:
        print_the_axis(axis)

def flatten_2d_axis(the_axis):
    return list(chain.from_iterable(the_axis))

def get_the_answer(filename, min_threshold=2):
    accumulator=0
    clean_data=parse_input_coordiantes(filename)
    xlarge, ylarge = find_hi_lo(clean_data)
    the_axis = create_xy_axis(xlarge, ylarge)
    for points in clean_data:
        add_line_to_axis(the_axis, points)
    flatten_the_axis = flatten_2d_axis(the_axis)    
    bad_threshold = max(flatten_the_axis)
    print('bad',bad_threshold)
    for value in range(min_threshold,bad_threshold+1):
        print(value)
        accumulator += flatten_the_axis.count(value)
    return accumulator


if __name__ == '__main__':
    puzzle_raw_data = r'./Python/Day 5/AoC_2021_d5_puzzle.data'
    somedata = get_the_answer(puzzle_raw_data)
    print(somedata)

    # sample_raw_data = r'./Python/Day 5/AoC_2021_d5_sample.data'
    # something = get_the_answer(sample_raw_data)
    # print(something)
    
    # clean_data=parse_input_coordiantes(sample_raw_data)
    # # [ print(_) for _ in clean_data]
    # xlarge, ylarge = find_hi_lo(clean_data)
    # # print(xlarge, ylarge)
    # x = create_xy_axis(xlarge, ylarge)
    # for points in clean_data:
    #     # print(points)
    #     add_line_to_axis(x, points)
    # # print_the_axis(x)
    # flat_sample = flatten_2d_axis(x)
    # assert 2 == max(flat_sample)
    # assert 5 == flat_sample.count(max(flat_sample))



    

