"""
Day 1 Sonar Sweep
Santa lost his sleigh keys in the drink.  Lets get ins a Submarine and fetch'm
Keys have a proximity system.  The tools range is: 0-50 stars
Goal, get all 50 stars by Dec 25th (Start day is Dec 1st)

Stars are collected by solving puzzles, each puzzle is one star.

Example input:
199
200
208
210
200
207
240
269
260
263


This report indicates that, scanning outward from the submarine, the sonar sweep found depths of 199, 200, 208, 210, and so on.

Report figure out how quickly the depth increases.

(1) count the number of times a depth measuremtn increases from the previous measurment.

Expected output with sample data:
199 (N/A - no previous measurement)
200 (increased)
208 (increased)
210 (increased)
200 (decreased)
207 (increased)
240 (increased)
269 (increased)
260 (decreased)
263 (increased)

In this example, there are 7 measurements that are larger than the previous measurement.

"""



class ThePuzzle_pt1:
    """
    This will create an object of the challenge and will generate information based on each falue of data that is fed into it
    Assumption:  each input of puzzle data is based on increments over time.
    """
    def __init__(self):
        """
        attribs:
            time_counter (based on counting number of data inputs)
            depth_measurement (data input from sonar)
        """
        self.time_counter = None
        self.previous_measurement = None
        self.delta_measurement = None
        self.increase_total = 0
        self.decrease_total = 0
        self.depth_measurement = []
        self.puzzle_dictionary = {}
        """
        {
            <time_int>:{
                depth:<depth_int>,
                delta:<int>
                change:<string>
            }
            }
        """
        self.increase_msg = "increased"
        self.decreased_msg = "decreased"
        self.first_measurement = "N/A - no previous measurement"
    
    def add_depth_reading(self, submarine_depth):
        """
        This represnts on time increment of data and will add it to list depth measurement
        """
        
        if self.previous_measurement == None: 
            # this is the first measurement
            self.time_counter = 0
            self.depth_measurement.append(submarine_depth)
            self.puzzle_dictionary[self.time_counter] = {
                "depth":submarine_depth, 
                "delta":None,
                "change":self.first_measurement
                }
            print(submarine_depth,self.first_measurement)
            self.previous_measurement = submarine_depth
            
        else:
            # increment timer
            self.time_counter += 1
            # current measurment is pr
            self.depth_measurement.append(submarine_depth)
            self.delta_measurement = self.depth_measurement[self.time_counter] - self.previous_measurement
            self.previous_measurement = self.depth_measurement[self.time_counter]
            # Add time : { depth: int, delta, int }
            self.puzzle_dictionary[self.time_counter] = {
                "depth":submarine_depth, 
                "delta":self.delta_measurement,
                "change":None
                }
            if self.delta_measurement > 0:
                # add time : { change: string}
                self.puzzle_dictionary[self.time_counter]["change"] = self.increase_msg
                # print(submarine_depth,self.increase_msg)
                self.increase_total += 1
            else:
                self.puzzle_dictionary[self.time_counter]["change"] = self.decreased_msg
                # print(submarine_depth,self.decreased_msg)
                self.decrease_total += 1
            
            self.previous_measurement = submarine_depth

    def print_log(self):
        """
        This will print out readings currently logged and if increasing or creasing
        """
        for time, value in enumerate(self.depth_measurement):
            print(time, value)
        print("-----------")
        # print()
        # for key, value in self.puzzle_dictionary.items():
        #     print(key,"|")
        #     for key2, value2, in value.items():
        #         print(key2,":",value2)

        print()
        print("-----------")
        print(self.puzzle_dictionary)
        print("Increase total: ", self.increase_total)
        print("Decrease total: ", self.decrease_total)
        


if __name__ == '__main__':
    """
    unit test using test data
    """
    testdata = [
        199,
        200,
        208,
        210,
        200,
        207,
        260,
        240,
        269,
        260,
        263
        ]
    # Create the object
    day1 = ThePuzzle_pt1()

    # input the data into the object
    for depth in testdata:
        day1.add_depth_reading(depth)

    if day1.increase_total == 7:
        print("Unit test Pass")
    else:
        print("Unit test failed!\n", "raw data:\n")
        day1.print_log()

