"""
Day 1 Sonar Sweep
Part 2

Instead, consider sums of a three-measurement sliding window. Again considering the above example:

199  A      
200  A B    
208  A B C  
210    B C D
200  E   C D
207  E F   D
240  E F G  
269    F G H
260      G H
263        H


Start by comparing the first and second three-measurement windows. The measurements in the first window are marked A (199, 200, 208); their sum is 199 + 200 + 208 = 607. The second window is marked B (200, 208, 210); its sum is 618. The sum of measurements in the second window is larger than the sum of the first, so this first comparison increased.

Your goal now is to count the number of times the sum of measurements in this sliding window increases from the previous sum. So, compare A with B, then compare B with C, then C with D, and so on. Stop when there aren't enough measurements left to create a new three-measurement sum.

In the above example, the sum of each three-measurement window is as follows:

A: 607 (N/A - no previous sum)
B: 618 (increased)
C: 618 (no change)
D: 617 (decreased)
E: 647 (increased)
F: 716 (increased)
G: 769 (increased)
H: 792 (increased)
In this example, there are 5 sums that are larger than the previous sum.

"""



class ThePuzzle_pt2:
    """
    This will create an object of the challenge and will generate information based on each falue of data that is fed into it
    Assumption:  each input of puzzle data is based on increments over time.
    """
    def __init__(self):
        """
        attribs:
            time_counter (based on counting number of data inputs)
            depth_measurement (data input from sonar)
        """
        self.time_counter = None
        self.previous_measurement = None
        self.delta_measurement = None
        self.increase_total = 0
        self.decrease_total = 0
        self.no_change_total = 0
        self.depth_measurement = []
        self.puzzle_dictionary = {}
        """
        {
            <time_int>:{
                depth:<depth_int>,
                delta:<int>
                change:<string>
            }
        }
        """
        self.qty_of_sums_dict = {}
        """
        {
            <qty_of_sum_int>:[<char>,<sumdepth>,<delta>,<msg>]
        }
        """

        self.increase_msg = "increased"
        self.decreased_msg = "decreased"
        self.no_change = "no change"
        self.first_measurement = "N/A - no previous measurement"
    
    def add_depth_reading(self, submarine_depth):
        """
        This represnts on time increment of data and will add it to list depth measurement
        """
        
        if self.previous_measurement == None: 
            # this is the first measurement
            self.time_counter = 0
            self.depth_measurement.append(submarine_depth)
            self.puzzle_dictionary[self.time_counter] = {
                "depth":submarine_depth, 
                "delta":None,
                "change":self.first_measurement
                }
            print(submarine_depth,self.first_measurement)
            self.previous_measurement = submarine_depth
            
        else:
            # increment timer
            self.time_counter += 1
            # current measurment is pr
            self.depth_measurement.append(submarine_depth)
            self.delta_measurement = self.depth_measurement[self.time_counter] - self.previous_measurement
            self.previous_measurement = self.depth_measurement[self.time_counter]
            # Add time : { depth: int, delta, int }
            self.puzzle_dictionary[self.time_counter] = {
                "depth":submarine_depth, 
                "delta":self.delta_measurement,
                "change":None
                }
            if self.delta_measurement > 0:
                # add time : { change: string}
                self.puzzle_dictionary[self.time_counter]["change"] = self.increase_msg
                # print(submarine_depth,self.increase_msg)
                self.increase_total += 1
            elif self.delta_measurement < 0:
                self.puzzle_dictionary[self.time_counter]["change"] = self.decreased_msg
                # print(submarine_depth,self.decreased_msg)
                self.decrease_total += 1
            else:
                self.puzzle_dictionary[self.time_counter]["change"] = self.no_change
                self.no_change_total += 1

        
            
            self.previous_measurement = submarine_depth

    def compare_by_sums_of_int(self, qty_of_sums):
        """
        qty_of_sums:  this is an int that will determin how many inputs to aggregate incrementally and compare with previous.
        self.qty_of_sums_dict[measure_window] will be added at the end
        """
        measure_window = qty_of_sums
        qty_of_sums_list = []
        ascii_int = ord("A")
        starting_ascii_int = ascii_int
        increasing = 0
        decreasing = 0
        no_change_count = 0
        for time, value in enumerate(self.depth_measurement):
            # print("Starting at: ", time, value)
            # if time == (measure_window -1):
            #     #define the starting sum of depth
            
            if (time + measure_window) <= (len(self.depth_measurement)):
                # no previous sum starting at "A" or chr(65)
                # list [<char>,<sumdepth>,<delta>,<msg>]
                qty_of_sums_list.append([chr(ascii_int), 0, None, None])

                # create block of summed depths
                for time2 in range(measure_window):
                    qty_of_sums_list[time][1] += self.depth_measurement[time2+time]
                if ascii_int == starting_ascii_int:
                   qty_of_sums_list[time][3] = self.first_measurement
                else:
                   qty_of_sums_list[time][2] = qty_of_sums_list[time][1] - qty_of_sums_list[time -1 ][1]
                   if qty_of_sums_list[time][2] > 0:
                    qty_of_sums_list[time][3] = self.increase_msg
                    increasing += 1
                   elif qty_of_sums_list[time][2] < 0:
                    qty_of_sums_list[time][3] = self.decreased_msg
                    decreasing += 1
                   else: # no change
                    qty_of_sums_list[time][3] = self.no_change
                    no_change_count += 1
                
                # print("completed ",chr(ascii_int),qty_of_sums_list[time])
                ascii_int += 1
        # for letter_list in qty_of_sums_list:
            # print(letter_list[0]+":",letter_list[1],letter_list[3])
        
        self.qty_of_sums_dict[qty_of_sums] = qty_of_sums_list
        return [increasing, decreasing, no_change_count]


        


    def print_log(self):
        """
        This will print out readings currently logged and if increasing or creasing
        """
        for time, value in enumerate(self.depth_measurement):
            print(time, value)
        print("-----------")
        # print()
        # for key, value in self.puzzle_dictionary.items():
        #     print(key,"|")
        #     for key2, value2, in value.items():
        #         print(key2,":",value2)

        print()
        print("-----------")
        print(self.puzzle_dictionary)
        print("Increase total: ", self.increase_total)
        print("Decrease total: ", self.decrease_total)
        print("No change total: ", self.no_change_total)
        


if __name__ == '__main__':
    """
    unit test using test data
    """
    testdata_pt1 = [
        199,
        200,
        208,
        210,
        200,
        207,
        260,
        240,
        269,
        260,
        263
        ]

        
    # Create the object
    day1_pt1 = ThePuzzle_pt2()
    """
    PART 1 TEST
    """
    # input the data into the object
    for depth in testdata_pt1:
        day1_pt1.add_depth_reading(depth)

    if day1_pt1.increase_total == 7:
        print("PT1 Unit test Pass")
    else:
        print("PT1 Unit test failed!\n", "raw data:\n")
        day1_pt1.print_log()
    """
    PART 2 TEST
    """
    testdata_pt2 = [
        199,
        200,
        208,
        210,
        200,
        207,
        240,
        269,
        260,
        263
        ]
    day1_pt2 = ThePuzzle_pt2()
    for depth in testdata_pt2:
        day1_pt2.add_depth_reading(depth)

    [inc_tst, dec_tst, nochang_tst] = day1_pt2.compare_by_sums_of_int(3)
    for letter_list in day1_pt2.qty_of_sums_dict[3]:
        print(letter_list[0]+":",letter_list[1],letter_list[3])
    print(f"{inc_tst} sums are larger than the previous sum")
    if inc_tst == 5:
        print("pt2 unit test passed")
    else:
        print(" part 2 unit test failed")
    

