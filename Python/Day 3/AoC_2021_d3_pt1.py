"""
Investigate creeky noizes with diagnostic check
use one binary numbers to two binary numbers for:
gammaRate
epsilonRate
powerConsumption = gammarate*epsilonRate


BitID 4 Gamma rate:
1:
    most common: 1
    5 false
    7 True
2:
    most common: 0
    7 false
    5 true

3:
    most common: 1
4
    most common: 1
5
    most common: 0

OR: 10110 = 22

BitID 4 epsilon:
    inverse: 01001 = 9



"""

# class TheScan:
#     """
#     This class will be used when doing diagnostic reports on Santa's submarine
#     """
#     def __init__(self) -> None:
#         """
#         initialize the object
#         """
#         self.gamma = None
#         self.epsilonrate = None

def add_data_to_counter(bit_stream_as_string, counter_bits=[]):
    """
    This will take a bit string of known length and process it with a known counter of number of True's or "1"s
    """
    bit_length = len(bit_stream_as_string)
    if len(counter_bits)==0:
        # initialize counter as none was provided.
        for position in range(bit_length):
            counter_bits.append(0)
    for position in range(bit_length):
        if int(bit_stream_as_string[position]) > 0:
            counter_bits[position] += 1

def data_cruncher(data_list):
    """
    Recieve a list of binary data as strings
    """
    data_counter=[]
    processed_size = 0
    for spot, data in enumerate(data_list):
        add_data_to_counter(data, data_counter)
        processed_size += 1
    # print(data_counter)
    return (data_counter, processed_size)

def give_me_rates(data_list):
    """
    This will take a counter of trues in binary data indexes and the size of data checked and spit out gamma and epsilon rates
    """
    (counter_of_trues,data_list_size) = data_cruncher(data_list)
    halfsize = data_list_size/2
    gamma_bits = []
    epsilon_bits = []
    for value in counter_of_trues:
        if value > halfsize:
            gamma_bits.append(1)
            epsilon_bits.append(0)
        else:
            gamma_bits.append(0)
            epsilon_bits.append(1)
    print(gamma_bits, epsilon_bits)
    gamma_rate = int("".join(str(_) for _ in gamma_bits),2)
    epsilon_rate = int("".join(str(_) for _ in epsilon_bits),2)
    print(f"""
    gamma_rate: {gamma_rate}
    epsilon_rate: {epsilon_rate}
    gamma_rate*epsilon_rate = {gamma_rate*epsilon_rate}
    """)
    return (gamma_rate, epsilon_rate)


if __name__ == '__main__':
    """
    Unit testing day 3 of AoC
    """
    testdata = [
    "00100",
    "11110",
    "10110",
    "10111",
    "10101",
    "01111",
    "00111",
    "11100",
    "10000",
    "11001",
    "00010",
    "01010"
    ]
    expected_result = [7, 5, 8, 7, 5]
    returned_tuple = data_cruncher(testdata)

    (gamma, epsilon) = give_me_rates(testdata)


    for index in range(len(returned_tuple[0])):
        assert returned_tuple[0][index] == expected_result[index]
        print(f"Index:{index} passed")
    print(type(gamma),type(epsilon))
    print(gamma,epsilon)
    print("Unit Test passed")
