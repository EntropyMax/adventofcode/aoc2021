"""
verify the life support rating, which can be determined by multiplying the oxygen generator rating by the CO2 scrubber rating.

life_support_rating = o2gen_rating * co2scrubber_rating

start with the full list of binary numbers from your
diagnostic report and consider just the first bit

-Keep only numbers selected by the bit criteria for the type of rating value for which you are searching. 
    Discard numbers which do not match the bit criteria.
-If you only have one number left, stop; this is the rating value for which you are searching.
-Otherwise, repeat the process, considering the next bit to the right.

bit criteria:
    o2gen: 
        (1) determin MOST COMMON value (0/1) in a bit position (1 always beats a TIE)
        (2) remove all data inputs that do not have the "common bit"  
        (3) repeat until only one data entry is available

    co2scrub:
        (1) determin the LEAST COMMON value in a bit position (0 always beats a TIE)
        (2) remove all data inputs that do not have the common bit
        (3) repeat until only one data entry is available

"""

from gettext import find
from multiprocessing import managers


def process_data_by_counter(bit_stream_as_string, counter_bits=[]):
    """
    This will take a bit string of known length and process it with a known counter of number of True's or "1"s
    """
    bit_length = len(bit_stream_as_string)
    if len(counter_bits)==0:
        # initialize counter as none was provided.
        for position in range(bit_length):
            counter_bits.append(0)
    for position in range(bit_length):
        if int(bit_stream_as_string[position]) > 0:
            counter_bits[position] += 1

def data_cruncher(data_list, print_stuff=False):
    """
    Recieve a list of binary data as strings
    """
    data_counter=[]
    processed_size = 0
    for spot, data in enumerate(data_list):
        process_data_by_counter(data, data_counter)
        processed_size += 1
    if print_stuff:
        print(data_counter)
    return (data_counter, processed_size)



def get_common_bits(data_list, print_stuff=False):
    """
    This will take a counter of trues in binary data indexes and the size of data checked and spit out gamma and epsilon rates
    """
    (counter_of_trues,data_list_size) = data_cruncher(data_list, print_stuff)
    halfsize = data_list_size/2
    major_bits = []
    minor_bits = []
    for value in counter_of_trues:
        if value >= halfsize:
            major_bits.append(1)
            minor_bits.append(0)
        else:
            major_bits.append(0)
            minor_bits.append(1)
    # print(major_bits, minor_bits)
    major= "".join(str(_) for _ in major_bits)
    minor = "".join(str(_) for _ in minor_bits)
    if print_stuff:
        print(f"""
    major: {major}
    minor: {minor}
    """)
    return (major, minor)

def filter_out_uncommon_data(data_list, bit_index, common_bit):
    """
    this receive a list and copy over to a new list numbers with the common bit value at that index
    common_bit comes in as a char/string
    bit index comes in as an integer
    """
    internal_list = []
    for string_data in data_list:
        if string_data[bit_index] == common_bit:
            internal_list.append(string_data)
    return internal_list

def find_the_rating(data_list, find_major=True, print_stuff=False):
    """
    This function will recieve a string list of binary numbers of the same bit size
    it will step through filtering out values from the list until the list is a size of one
    and then print and return that value in base 10 format.
    """
    # Get the major/minor 
    common_bits_list = get_common_bits(data_list, print_stuff)
    bit_string_size = len(common_bits_list[0])
    data_list_size = len(data_list)
    if find_major == True:
        common_bits_index = 0
    else:
        common_bits_index = 1
    processed_list = list(data_list)
    #  copy the input data_list and then continue to process until it is of one size
    while len(processed_list) > 1:    
        if print_stuff:
            print("outside","data_list_size",data_list_size, processed_list)
        for bit_index in range(bit_string_size):
            if print_stuff:
                print("inside","data_list_size",data_list_size, bit_index,common_bits_list[common_bits_index], processed_list)
            if data_list_size > 1:
                processed_list = filter_out_uncommon_data(processed_list, bit_index, common_bits_list[common_bits_index][bit_index])
                common_bits_list = get_common_bits(processed_list, print_stuff)
            data_list_size = len(processed_list)
    if data_list_size > 0 and print_stuff:
        print(processed_list)
        print(processed_list[0])
        print(int(processed_list[0],2))
    return int(processed_list[0],2)

def print_the_answer(data_list, print_stuff=False):
    """
    This simply calls the functions to get the answers and prints them out.
    """
    o2 = find_the_rating(data_list, True)
    co2 =find_the_rating(data_list, False)
    print(f"""
    O2 rating: {o2}
    CO2 scrubber rating: {co2}
    Life support rating: {o2*co2}
    """
    )



if __name__ == '__main__':
    """
    Unit testing day 3 of AoC
    """
    testdata = [
    "00100",
    "11110",
    "10110",
    "10111",
    "10101",
    "01111",
    "00111",
    "11100",
    "10000",
    "11001",
    "00010",
    "01010"
    ]
    expected_result = [7, 5, 8, 7, 5]
    returned_tuple = data_cruncher(testdata)

    (major, minor) = get_common_bits(testdata)

    for index in range(len(returned_tuple[0])):
        assert returned_tuple[0][index] == expected_result[index]
        print(f"Index:{index} passed")
    print(type(major),type(minor))
    print(major,minor)
    print("Part 1 Unit Test passed")

    # Start PART 2 Unit testing
    o2 = find_the_rating(testdata, True)
    assert o2 == 23
    co2 = find_the_rating(testdata, False)
    assert co2 == 10
    assert co2*o2 == 230

    print_the_answer(testdata)
    print("Part 2 Unit Test Pass")

