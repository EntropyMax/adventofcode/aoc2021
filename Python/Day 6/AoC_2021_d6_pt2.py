"""
So, suppose you have a lanternfish with an internal timer value of 3:

After one day, its internal timer would become 2.
After another day, its internal timer would become 1.
After another day, its internal timer would become 0.
After another day, its internal timer would reset to 6, and it would create a new lanternfish with an internal timer of 8.
After another day, the first lanternfish would have an internal timer of 5, and the second lanternfish would have an internal timer of 7.

Initial state: 3,4,3,1,2
After  1 day:  2,3,2,0,1
After  2 days: 1,2,1,6,0,8
After  3 days: 0,1,0,5,6,7,8
After  4 days: 6,0,6,4,5,6,7,8,8
After  5 days: 5,6,5,3,4,5,6,7,7,8
After  6 days: 4,5,4,2,3,4,5,6,6,7
After  7 days: 3,4,3,1,2,3,4,5,5,6
After  8 days: 2,3,2,0,1,2,3,4,4,5
After  9 days: 1,2,1,6,0,1,2,3,3,4,8
After 10 days: 0,1,0,5,6,0,1,2,2,3,7,8
After 11 days: 6,0,6,4,5,6,0,1,1,2,6,7,8,8,8
After 12 days: 5,6,5,3,4,5,6,0,0,1,5,6,7,7,7,8,8
After 13 days: 4,5,4,2,3,4,5,6,6,0,4,5,6,6,6,7,7,8,8
After 14 days: 3,4,3,1,2,3,4,5,5,6,3,4,5,5,5,6,6,7,7,8
After 15 days: 2,3,2,0,1,2,3,4,4,5,2,3,4,4,4,5,5,6,6,7
After 16 days: 1,2,1,6,0,1,2,3,3,4,1,2,3,3,3,4,4,5,5,6,8
After 17 days: 0,1,0,5,6,0,1,2,2,3,0,1,2,2,2,3,3,4,4,5,7,8
After 18 days: 6,0,6,4,5,6,0,1,1,2,6,0,1,1,1,2,2,3,3,4,6,7,8,8,8,8


[[3,4,3,1,2]]
[[2,3,2,0,1]]
[[1,2,1,6,0,8]]
[[0,1,0,5,6,7,8]]
[[6,0,6,4,5,6,7,8,8]]
[[5,6,5,3,4,5,6,7,7,8]]
[[4,5,4,2,3,4,5,6,6,7]]
[[3,4,3,1,2,3,4,5,5,6]]
[[2,3,2,0,1,2,3,4,4,5]]
[[1,2,1,6,0,1,2,3,3,4,8]]
[[0,1,0,5,6,0,1,2,2,3,7,8]]
[[6,0,6,4,5,6,0,1,1,2,6,7,8,8,8]]
[[5,6,5,3,4,5,6,0,0,1,5,6,7,7,7,8,8]]
[[4,5,4,2,3,4,5,6,6,0,4,5,6,6,6,7,7,8,8]]
[[3,4,3,1,2,3,4,5,5,6,3,4,5,5,5,6,6,7,7,8]]
[[2,3,2,0,1,2,3,4,4,5,2,3,4,4,4,5,5,6,6,7]]
[[1,2,1,6,0,1,2,3,3,4,1,2,3,3,3,4,4,5,5,6,8]]
[[0,1,0,5,6,0,1,2,2,3,0,1,2,2,2,3,3,4,4,5,7,8]]
[[6,0,6,4,5,6,0,1,1,2,6,0,1,1,1,2,2,3,3,4,6,7,8,8,8,8]]


    --- Part Two ---
    Suppose the lanternfish live forever and have unlimited food and space. Would they take over the entire ocean?

    After 256 days in the example above, there would be a total of 26984457539 lanternfish!


Lets create a starting age group and based on the size of the age group add more fish when they get to zero.


"""

def initialize_age_group_table(starting_fish_list):
    """
    Sample data was 3,4,3,1,2

    we can do a linear table in which each column represents an age
    with the value of each age having a "count" of common fish at that age.

    whenever a column hits age zero, that count will be added to an "8" column and the zero will be labeled 6

    0 1 2 3 4 5 6 7 8  <--- age of a group of fish
    0 1 1 2 1 0 0 0 0  <--- count of fish at that age number
    """

    processed_fish_groups = [
        [0,0],
        [1,0],
        [2,0],
        [3,0],
        [4,0],
        [5,0],
        [6,0],
        [7,0],
        [8,0]
        ]

    for fish_age in starting_fish_list:
        processed_fish_groups[fish_age][1] += 1
    processed_fish_groups.sort()
    return processed_fish_groups

def main(staring_fish_list, day_stop_counting, printme=None):
    """
    take in raw data and categorize by counts of fish at various starting ages.
    """
    fish_grouped_by_age = initialize_age_group_table(staring_fish_list)
    if printme:
        print("Start:\t", fish_grouped_by_age)
    for day in range(day_stop_counting):
        # print(day)
        # print(fish_grouped_by_age)
        add_one_day_list(fish_grouped_by_age)
    
    num_fish=0
    for fish_group in fish_grouped_by_age:
        num_fish += fish_group[1]
    if printme:
        print("End:\t",fish_grouped_by_age)
        print(num_fish)
    return num_fish

    

def add_one_day_list(fish_grouped_by_age, reset_number=6, newborn_num=8):
    """
    takes test data as a list and processes the list as if a new day has been added to it.
    """

    add_age_fish_reset = 0
    # make sure age chunks are sorted
    fish_grouped_by_age.sort()
    fish_grouped_by_age[0][0] = newborn_num
    add_age_fish_reset = fish_grouped_by_age[0][1]

    for index in range(1, reset_number + 1):
        fish_grouped_by_age[index][0] -= 1

    # need to add fish that just gave birth with fish getting ready to have first child
    fish_grouped_by_age[reset_number+1][0] -= 1
    fish_grouped_by_age[reset_number+1][1] += add_age_fish_reset

    # complete the countdowns
    for index in range(reset_number+2, newborn_num+1):
        fish_grouped_by_age[index][0] -= 1
    fish_grouped_by_age.sort()

if __name__ == "__main__":
    """
    run some unit tests
    """
    start_test_fish_list = [3,4,3,1,2]
    # initialize_age_group_table(start_test_fish_list)
    # processed_fish_list = list(start_test_fish_list)
    print("checking 18 days makes 26 fish")
    print(main(list(start_test_fish_list), 18))
    # assert main(list(start_test_fish_list), 18) == 26
    print("checking 80 days makes 5934 fish")
    print(main(list(start_test_fish_list), 80))
    # assert main(start_test_fish_list, 80) == 5934
    print("checking 256 days make 26984457539 fish ")
    print(main(list(start_test_fish_list), 256))
    # assert main(start_test_fish_list, 256) == 26984457539
    print("unit test sat")

    print("Checking part 1 puzzle input makes 352151 fish after 80 days ")
    puzzle_input = [
        2,5,5,3,2,2,5,1,4,5,2,1,5,5,1,2,3,3,4,1,4,1,4,4,2,1,5,5,3,5,4,3,4,1,5,4,1,5,5,5,4,3,1,2,1,5,1,4,4,1,4,1,3,1,1,1,3,1,1,2,1,3,1,1,1,2,3,5,5,3,2,3,3,2,2,1,3,1,3,1,5,5,1,2,3,2,1,1,2,1,2,1,2,2,1,3,5,4,3,3,2,2,3,1,4,2,2,1,3,4,5,4,2,5,4,1,2,1,3,5,3,3,5,4,1,1,5,2,4,4,1,2,2,5,5,3,1,2,4,3,3,1,4,2,5,1,5,1,2,1,1,1,1,3,5,5,1,5,5,1,2,2,1,2,1,2,1,2,1,4,5,1,2,4,3,3,3,1,5,3,2,2,1,4,2,4,2,3,2,5,1,5,1,1,1,3,1,1,3,5,4,2,5,3,2,2,1,4,5,1,3,2,5,1,2,1,4,1,5,5,1,2,2,1,2,4,5,3,3,1,4,4,3,1,4,2,4,4,3,4,1,4,5,3,1,4,2,2,3,4,4,4,1,4,3,1,3,4,5,1,5,4,4,4,5,5,5,2,1,3,4,3,2,5,3,1,3,2,2,3,1,4,5,3,5,5,3,2,3,1,2,5,2,1,3,1,1,1,5,1
        ]
    initialize_age_group_table
    print(main(puzzle_input, 80))
    # Get answer for part II
    print(main(puzzle_input, 256))
