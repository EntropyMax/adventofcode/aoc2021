"""
So, suppose you have a lanternfish with an internal timer value of 3:

After one day, its internal timer would become 2.
After another day, its internal timer would become 1.
After another day, its internal timer would become 0.
After another day, its internal timer would reset to 6, and it would create a new lanternfish with an internal timer of 8.
After another day, the first lanternfish would have an internal timer of 5, and the second lanternfish would have an internal timer of 7.

Initial state: 3,4,3,1,2
After  1 day:  2,3,2,0,1
After  2 days: 1,2,1,6,0,8
After  3 days: 0,1,0,5,6,7,8
After  4 days: 6,0,6,4,5,6,7,8,8
After  5 days: 5,6,5,3,4,5,6,7,7,8
After  6 days: 4,5,4,2,3,4,5,6,6,7
After  7 days: 3,4,3,1,2,3,4,5,5,6
After  8 days: 2,3,2,0,1,2,3,4,4,5
After  9 days: 1,2,1,6,0,1,2,3,3,4,8
After 10 days: 0,1,0,5,6,0,1,2,2,3,7,8
After 11 days: 6,0,6,4,5,6,0,1,1,2,6,7,8,8,8
After 12 days: 5,6,5,3,4,5,6,0,0,1,5,6,7,7,7,8,8
After 13 days: 4,5,4,2,3,4,5,6,6,0,4,5,6,6,6,7,7,8,8
After 14 days: 3,4,3,1,2,3,4,5,5,6,3,4,5,5,5,6,6,7,7,8
After 15 days: 2,3,2,0,1,2,3,4,4,5,2,3,4,4,4,5,5,6,6,7
After 16 days: 1,2,1,6,0,1,2,3,3,4,1,2,3,3,3,4,4,5,5,6,8
After 17 days: 0,1,0,5,6,0,1,2,2,3,0,1,2,2,2,3,3,4,4,5,7,8
After 18 days: 6,0,6,4,5,6,0,1,1,2,6,0,1,1,1,2,2,3,3,4,6,7,8,8,8,8


[[3,4,3,1,2]]
[[2,3,2,0,1]]
[[1,2,1,6,0,8]]
[[0,1,0,5,6,7,8]]
[[6,0,6,4,5,6,7,8,8]]
[[5,6,5,3,4,5,6,7,7,8]]
[[4,5,4,2,3,4,5,6,6,7]]
[[3,4,3,1,2,3,4,5,5,6]]
[[2,3,2,0,1,2,3,4,4,5]]
[[1,2,1,6,0,1,2,3,3,4,8]]
[[0,1,0,5,6,0,1,2,2,3,7,8]]
[[6,0,6,4,5,6,0,1,1,2,6,7,8,8,8]]
[[5,6,5,3,4,5,6,0,0,1,5,6,7,7,7,8,8]]
[[4,5,4,2,3,4,5,6,6,0,4,5,6,6,6,7,7,8,8]]
[[3,4,3,1,2,3,4,5,5,6,3,4,5,5,5,6,6,7,7,8]]
[[2,3,2,0,1,2,3,4,4,5,2,3,4,4,4,5,5,6,6,7]]
[[1,2,1,6,0,1,2,3,3,4,1,2,3,3,3,4,4,5,5,6,8]]
[[0,1,0,5,6,0,1,2,2,3,0,1,2,2,2,3,3,4,4,5,7,8]]
[[6,0,6,4,5,6,0,1,1,2,6,0,1,1,1,2,2,3,3,4,6,7,8,8,8,8]]
"""

def main(start_data_as_list, day_stop_counting, printme=None):
    """
    This will receive a list of numbers that count down to zero
    when they get to zero, they will reset to the number six and 
    add another number to the list with a value of 8 and continue 
    to count down to zero

    The output would print out total lanternfish that exist
    """
    processing_list = list(start_data_as_list)
    if printme:
        print("Starting List Data: ", start_data_as_list, " Days to be applied: ", day_stop_counting)
    for day in range(day_stop_counting):
        processing_list = add_one_day_list(processing_list, 6)
    if printme:
        print("  Ending List Data: ", processing_list)
    return processing_list

def add_one_day_list(fish_list, reset_number=6):
    """
    takes test data as a list and processes the list as if a new day has been added to it.
    """
    processing_fish_list = list(fish_list)
    for index, fish_countdowner in enumerate(fish_list):
        if fish_countdowner < 1:
            # reset fish to reset_number
            processing_fish_list[index] = reset_number
            # spawn a fish
            spawn_a_fish(processing_fish_list, starting_number=8)
        else:
            # count down one
            processing_fish_list[index] -= 1
    return processing_fish_list

def spawn_a_fish(add_fish_list, starting_number=8):
    """
    When provided a list, it will append a value to this list 
    and initialize it to the starting number provided
    """
    add_fish_list.append(starting_number)

if __name__ == "__main__":
    """
    run some unit tests
    """
    test_data_list_of_fish_list = [
        [3,4,3,1,2],
        [2,3,2,0,1],
        [1,2,1,6,0,8],
        [0,1,0,5,6,7,8],
        [6,0,6,4,5,6,7,8,8],
        [5,6,5,3,4,5,6,7,7,8],
        [4,5,4,2,3,4,5,6,6,7],
        [3,4,3,1,2,3,4,5,5,6],
        [2,3,2,0,1,2,3,4,4,5],
        [1,2,1,6,0,1,2,3,3,4,8],
        [0,1,0,5,6,0,1,2,2,3,7,8],
        [6,0,6,4,5,6,0,1,1,2,6,7,8,8,8],
        [5,6,5,3,4,5,6,0,0,1,5,6,7,7,7,8,8],
        [4,5,4,2,3,4,5,6,6,0,4,5,6,6,6,7,7,8,8],
        [3,4,3,1,2,3,4,5,5,6,3,4,5,5,5,6,6,7,7,8],
        [2,3,2,0,1,2,3,4,4,5,2,3,4,4,4,5,5,6,6,7],
        [1,2,1,6,0,1,2,3,3,4,1,2,3,3,3,4,4,5,5,6,8],
        [0,1,0,5,6,0,1,2,2,3,0,1,2,2,2,3,3,4,4,5,7,8],
        [6,0,6,4,5,6,0,1,1,2,6,0,1,1,1,2,2,3,3,4,6,7,8,8,8,8]
    ]
    start_test_fish_list = [3,4,3,1,2]
    assert test_data_list_of_fish_list[3], main(start_test_fish_list, 3)
    for index, test_day_list in enumerate(test_data_list_of_fish_list):
        if index == len(test_data_list_of_fish_list)-1:
            continue
        # print(test_data_list_of_fish_list[index+1], main(test_day_list, 1))
        assert (test_data_list_of_fish_list[index+1] == main(test_day_list, 1))
    the_end = len(test_data_list_of_fish_list)
    # print(main(start_test_fish_list, the_end-1))
    # print(test_data_list_of_fish_list[the_end-1])
    assert (main(start_test_fish_list, the_end-1) == test_data_list_of_fish_list[the_end-1])

    """
    In this example, after 18 days, there are a total of 26 fish. After 80 days, there would be a total of 5934.

    """
    print(len(main(start_test_fish_list, 18)))
    assert len(main(start_test_fish_list, 18)) == 26
    print(len(main(start_test_fish_list, 80)))
    assert len(main(start_test_fish_list, 80)) == 5934


    print("unit tests passed!")

    puzzle_input = [2,5,5,3,2,2,5,1,4,5,2,1,5,5,1,2,3,3,4,1,4,1,4,4,2,1,5,5,3,5,4,3,4,1,5,4,1,5,5,5,4,3,1,2,1,5,1,4,4,1,4,1,3,1,1,1,3,1,1,2,1,3,1,1,1,2,3,5,5,3,2,3,3,2,2,1,3,1,3,1,5,5,1,2,3,2,1,1,2,1,2,1,2,2,1,3,5,4,3,3,2,2,3,1,4,2,2,1,3,4,5,4,2,5,4,1,2,1,3,5,3,3,5,4,1,1,5,2,4,4,1,2,2,5,5,3,1,2,4,3,3,1,4,2,5,1,5,1,2,1,1,1,1,3,5,5,1,5,5,1,2,2,1,2,1,2,1,2,1,4,5,1,2,4,3,3,3,1,5,3,2,2,1,4,2,4,2,3,2,5,1,5,1,1,1,3,1,1,3,5,4,2,5,3,2,2,1,4,5,1,3,2,5,1,2,1,4,1,5,5,1,2,2,1,2,4,5,3,3,1,4,4,3,1,4,2,4,4,3,4,1,4,5,3,1,4,2,2,3,4,4,4,1,4,3,1,3,4,5,1,5,4,4,4,5,5,5,2,1,3,4,3,2,5,3,1,3,2,2,3,1,4,5,3,5,5,3,2,3,1,2,5,2,1,3,1,1,1,5,1]
    result = main(puzzle_input, 80)
    print(len(result))
